package com.adeo.bomp.deliverysimulation.controller;

import com.adeo.bomp.deliverysimulation.services.DeliverySimulationService;
import com.adeo.bomp.deliverysimulation.swagger.api.DeliverysimulationsApi;
import com.adeo.bomp.deliverysimulation.swagger.model.DeliverySimulationRequest;
import com.adeo.bomp.deliverysimulation.swagger.model.DeliverySimulationResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T13:48:48.685Z")

@Controller
public class DeliverysimulationsApiController implements DeliverysimulationsApi {
    @Autowired
    DeliverySimulationService deliverySimulationService;

    private static final Logger log = LoggerFactory.getLogger(DeliverysimulationsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public DeliverysimulationsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<DeliverySimulationResult> createDeliverySimulation(@ApiParam(value = "Cart we need to simulate" ,required=true )  @Valid @RequestBody DeliverySimulationRequest body) {
        System.out.println(body);
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<DeliverySimulationResult>(objectMapper.readValue("{  \"solutions\" : [ {    \"test\" : \"test\"  }, {    \"test\" : \"test\"  } ]}", DeliverySimulationResult.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<DeliverySimulationResult>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<DeliverySimulationResult>(HttpStatus.NOT_IMPLEMENTED);
    }

}
