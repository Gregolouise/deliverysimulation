package com.adeo.bomp.deliverysimulation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
public class DeliverySimulationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliverySimulationApplication.class, args);
	}


}
