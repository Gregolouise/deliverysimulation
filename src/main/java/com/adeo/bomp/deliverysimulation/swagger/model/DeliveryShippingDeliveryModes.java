package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * DeliveryShippingDeliveryModes
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T15:13:26.501Z")

public class DeliveryShippingDeliveryModes   {
  @JsonProperty("deliveryMode")
  private String deliveryMode = null;

  @JsonProperty("shippingOptionReference")
  private String shippingOptionReference = null;

  @JsonProperty("deliveryDate")
  private String deliveryDate = null;

  @JsonProperty("price")
  private DeliveryShippingDeliveryModesPrice price = null;

  public DeliveryShippingDeliveryModes deliveryMode(String deliveryMode) {
    this.deliveryMode = deliveryMode;
    return this;
  }

  /**
   * Get deliveryMode
   * @return deliveryMode
  **/
  @ApiModelProperty(value = "")


  public String getDeliveryMode() {
    return deliveryMode;
  }

  public void setDeliveryMode(String deliveryMode) {
    this.deliveryMode = deliveryMode;
  }

  public DeliveryShippingDeliveryModes shippingOptionReference(String shippingOptionReference) {
    this.shippingOptionReference = shippingOptionReference;
    return this;
  }

  /**
   * Get shippingOptionReference
   * @return shippingOptionReference
  **/
  @ApiModelProperty(value = "")


  public String getShippingOptionReference() {
    return shippingOptionReference;
  }

  public void setShippingOptionReference(String shippingOptionReference) {
    this.shippingOptionReference = shippingOptionReference;
  }

  public DeliveryShippingDeliveryModes deliveryDate(String deliveryDate) {
    this.deliveryDate = deliveryDate;
    return this;
  }

  /**
   * Get deliveryDate
   * @return deliveryDate
  **/
  @ApiModelProperty(value = "")


  public String getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(String deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public DeliveryShippingDeliveryModes price(DeliveryShippingDeliveryModesPrice price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DeliveryShippingDeliveryModesPrice getPrice() {
    return price;
  }

  public void setPrice(DeliveryShippingDeliveryModesPrice price) {
    this.price = price;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryShippingDeliveryModes deliveryShippingDeliveryModes = (DeliveryShippingDeliveryModes) o;
    return Objects.equals(this.deliveryMode, deliveryShippingDeliveryModes.deliveryMode) &&
        Objects.equals(this.shippingOptionReference, deliveryShippingDeliveryModes.shippingOptionReference) &&
        Objects.equals(this.deliveryDate, deliveryShippingDeliveryModes.deliveryDate) &&
        Objects.equals(this.price, deliveryShippingDeliveryModes.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deliveryMode, shippingOptionReference, deliveryDate, price);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryShippingDeliveryModes {\n");
    
    sb.append("    deliveryMode: ").append(toIndentedString(deliveryMode)).append("\n");
    sb.append("    shippingOptionReference: ").append(toIndentedString(shippingOptionReference)).append("\n");
    sb.append("    deliveryDate: ").append(toIndentedString(deliveryDate)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

