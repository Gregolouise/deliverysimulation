package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * DeliveryShippingItems
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T15:13:26.501Z")

public class DeliveryShippingItems   {
  @JsonProperty("offers")
  @Valid
  private List<DeliveryShippingItemsOffer> offers = null;

  @JsonProperty("deliveryModes")
  @Valid
  private List<DeliveryShippingDeliveryModes> deliveryModes = null;

  @JsonProperty("deliveryShippingServiceLevels")
  private DeliveryShippingItemsDeliveryShippingServiceLevels deliveryShippingServiceLevels = null;

  public DeliveryShippingItems offers(List<DeliveryShippingItemsOffer> offers) {
    this.offers = offers;
    return this;
  }

  public DeliveryShippingItems addOffersItem(DeliveryShippingItemsOffer offersItem) {
    if (this.offers == null) {
      this.offers = new ArrayList<DeliveryShippingItemsOffer>();
    }
    this.offers.add(offersItem);
    return this;
  }

  /**
   * Get offers
   * @return offers
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DeliveryShippingItemsOffer> getOffers() {
    return offers;
  }

  public void setOffers(List<DeliveryShippingItemsOffer> offers) {
    this.offers = offers;
  }

  public DeliveryShippingItems deliveryModes(List<DeliveryShippingDeliveryModes> deliveryModes) {
    this.deliveryModes = deliveryModes;
    return this;
  }

  public DeliveryShippingItems addDeliveryModesItem(DeliveryShippingDeliveryModes deliveryModesItem) {
    if (this.deliveryModes == null) {
      this.deliveryModes = new ArrayList<DeliveryShippingDeliveryModes>();
    }
    this.deliveryModes.add(deliveryModesItem);
    return this;
  }

  /**
   * Get deliveryModes
   * @return deliveryModes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DeliveryShippingDeliveryModes> getDeliveryModes() {
    return deliveryModes;
  }

  public void setDeliveryModes(List<DeliveryShippingDeliveryModes> deliveryModes) {
    this.deliveryModes = deliveryModes;
  }

  public DeliveryShippingItems deliveryShippingServiceLevels(DeliveryShippingItemsDeliveryShippingServiceLevels deliveryShippingServiceLevels) {
    this.deliveryShippingServiceLevels = deliveryShippingServiceLevels;
    return this;
  }

  /**
   * Get deliveryShippingServiceLevels
   * @return deliveryShippingServiceLevels
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DeliveryShippingItemsDeliveryShippingServiceLevels getDeliveryShippingServiceLevels() {
    return deliveryShippingServiceLevels;
  }

  public void setDeliveryShippingServiceLevels(DeliveryShippingItemsDeliveryShippingServiceLevels deliveryShippingServiceLevels) {
    this.deliveryShippingServiceLevels = deliveryShippingServiceLevels;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryShippingItems deliveryShippingItems = (DeliveryShippingItems) o;
    return Objects.equals(this.offers, deliveryShippingItems.offers) &&
        Objects.equals(this.deliveryModes, deliveryShippingItems.deliveryModes) &&
        Objects.equals(this.deliveryShippingServiceLevels, deliveryShippingItems.deliveryShippingServiceLevels);
  }

  @Override
  public int hashCode() {
    return Objects.hash(offers, deliveryModes, deliveryShippingServiceLevels);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryShippingItems {\n");
    
    sb.append("    offers: ").append(toIndentedString(offers)).append("\n");
    sb.append("    deliveryModes: ").append(toIndentedString(deliveryModes)).append("\n");
    sb.append("    deliveryShippingServiceLevels: ").append(toIndentedString(deliveryShippingServiceLevels)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

