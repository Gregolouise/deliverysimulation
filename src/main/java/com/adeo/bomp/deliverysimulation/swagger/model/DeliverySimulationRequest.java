package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Request of simulation for a User Cart
 */
@ApiModel(description = "Request of simulation for a User Cart")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T13:48:48.685Z")

public class DeliverySimulationRequest   {
  @JsonProperty("offers")
  @Valid
  private List<OfferCartItem> offers = null;

  @JsonProperty("deliveryplace")
  private DeliveryPlace deliveryplace = null;

  public DeliverySimulationRequest offers(List<OfferCartItem> offers) {
    this.offers = offers;
    return this;
  }

  public DeliverySimulationRequest addOffersItem(OfferCartItem offersItem) {
    if (this.offers == null) {
      this.offers = new ArrayList<OfferCartItem>();
    }
    this.offers.add(offersItem);
    return this;
  }

  /**
   * Get offers
   * @return offers
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OfferCartItem> getOffers() {
    return offers;
  }

  public void setOffers(List<OfferCartItem> offers) {
    this.offers = offers;
  }

  public DeliverySimulationRequest deliveryplace(DeliveryPlace deliveryplace) {
    this.deliveryplace = deliveryplace;
    return this;
  }

  /**
   * Get deliveryplace
   * @return deliveryplace
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DeliveryPlace getDeliveryplace() {
    return deliveryplace;
  }

  public void setDeliveryplace(DeliveryPlace deliveryplace) {
    this.deliveryplace = deliveryplace;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliverySimulationRequest deliverySimulationRequest = (DeliverySimulationRequest) o;
    return Objects.equals(this.offers, deliverySimulationRequest.offers) &&
        Objects.equals(this.deliveryplace, deliverySimulationRequest.deliveryplace);
  }

  @Override
  public int hashCode() {
    return Objects.hash(offers, deliveryplace);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliverySimulationRequest {\n");
    
    sb.append("    offers: ").append(toIndentedString(offers)).append("\n");
    sb.append("    deliveryplace: ").append(toIndentedString(deliveryplace)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

