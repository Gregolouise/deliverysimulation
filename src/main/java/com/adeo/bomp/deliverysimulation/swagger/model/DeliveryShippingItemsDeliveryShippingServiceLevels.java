package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * DeliveryShippingItemsDeliveryShippingServiceLevels
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T15:13:26.501Z")

public class DeliveryShippingItemsDeliveryShippingServiceLevels   {
  @JsonProperty("code")
  private String code = null;

  @JsonProperty("name")
  private String name = null;

  public DeliveryShippingItemsDeliveryShippingServiceLevels code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Get code
   * @return code
  **/
  @ApiModelProperty(value = "")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public DeliveryShippingItemsDeliveryShippingServiceLevels name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryShippingItemsDeliveryShippingServiceLevels deliveryShippingItemsDeliveryShippingServiceLevels = (DeliveryShippingItemsDeliveryShippingServiceLevels) o;
    return Objects.equals(this.code, deliveryShippingItemsDeliveryShippingServiceLevels.code) &&
        Objects.equals(this.name, deliveryShippingItemsDeliveryShippingServiceLevels.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryShippingItemsDeliveryShippingServiceLevels {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

