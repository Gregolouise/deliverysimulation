package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * DeliveryShipping
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T15:13:26.501Z")

public class DeliveryShipping   {
  @JsonProperty("deliveryShipping")
  @Valid
  private List<DeliveryShippingItems> deliveryShipping = null;

  public DeliveryShipping deliveryShipping(List<DeliveryShippingItems> deliveryShipping) {
    this.deliveryShipping = deliveryShipping;
    return this;
  }

  public DeliveryShipping addDeliveryShippingItem(DeliveryShippingItems deliveryShippingItem) {
    if (this.deliveryShipping == null) {
      this.deliveryShipping = new ArrayList<DeliveryShippingItems>();
    }
    this.deliveryShipping.add(deliveryShippingItem);
    return this;
  }

  /**
   * Get deliveryShipping
   * @return deliveryShipping
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DeliveryShippingItems> getDeliveryShipping() {
    return deliveryShipping;
  }

  public void setDeliveryShipping(List<DeliveryShippingItems> deliveryShipping) {
    this.deliveryShipping = deliveryShipping;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryShipping deliveryShipping = (DeliveryShipping) o;
    return Objects.equals(this.deliveryShipping, deliveryShipping.deliveryShipping);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deliveryShipping);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryShipping {\n");
    
    sb.append("    deliveryShipping: ").append(toIndentedString(deliveryShipping)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

