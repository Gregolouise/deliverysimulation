package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Offer part of a cart item.
 */
@ApiModel(description = "Offer part of a cart item.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T13:48:48.685Z")

public class OfferCartItem   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("productId")
  private String productId = null;

  @JsonProperty("offerId")
  private String offerId = null;

  @JsonProperty("vendorId")
  private String vendorId = null;

  @JsonProperty("quantity")
  private BigDecimal quantity = null;

  public OfferCartItem id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Line ID.
   * @return id
  **/
  @ApiModelProperty(value = "Line ID.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OfferCartItem productId(String productId) {
    this.productId = productId;
    return this;
  }

  /**
   * Product ID.
   * @return productId
  **/
  @ApiModelProperty(value = "Product ID.")


  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public OfferCartItem offerId(String offerId) {
    this.offerId = offerId;
    return this;
  }

  /**
   * Offer ID.
   * @return offerId
  **/
  @ApiModelProperty(value = "Offer ID.")


  public String getOfferId() {
    return offerId;
  }

  public void setOfferId(String offerId) {
    this.offerId = offerId;
  }

  public OfferCartItem vendorId(String vendorId) {
    this.vendorId = vendorId;
    return this;
  }

  /**
   * Vendor ID.
   * @return vendorId
  **/
  @ApiModelProperty(value = "Vendor ID.")


  public String getVendorId() {
    return vendorId;
  }

  public void setVendorId(String vendorId) {
    this.vendorId = vendorId;
  }

  public OfferCartItem quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Number of products added to the offer line.
   * @return quantity
  **/
  @ApiModelProperty(value = "Number of products added to the offer line.")

  @Valid

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OfferCartItem offerCartItem = (OfferCartItem) o;
    return Objects.equals(this.id, offerCartItem.id) &&
        Objects.equals(this.productId, offerCartItem.productId) &&
        Objects.equals(this.offerId, offerCartItem.offerId) &&
        Objects.equals(this.vendorId, offerCartItem.vendorId) &&
        Objects.equals(this.quantity, offerCartItem.quantity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, productId, offerId, vendorId, quantity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OfferCartItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    offerId: ").append(toIndentedString(offerId)).append("\n");
    sb.append("    vendorId: ").append(toIndentedString(vendorId)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

