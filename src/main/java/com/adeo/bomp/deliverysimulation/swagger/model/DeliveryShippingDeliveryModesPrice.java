package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * DeliveryShippingDeliveryModesPrice
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T15:13:26.501Z")

public class DeliveryShippingDeliveryModesPrice   {
  @JsonProperty("amout")
  private Integer amout = null;

  @JsonProperty("currency")
  private String currency = null;

  public DeliveryShippingDeliveryModesPrice amout(Integer amout) {
    this.amout = amout;
    return this;
  }

  /**
   * Get amout
   * @return amout
  **/
  @ApiModelProperty(value = "")


  public Integer getAmout() {
    return amout;
  }

  public void setAmout(Integer amout) {
    this.amout = amout;
  }

  public DeliveryShippingDeliveryModesPrice currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryShippingDeliveryModesPrice deliveryShippingDeliveryModesPrice = (DeliveryShippingDeliveryModesPrice) o;
    return Objects.equals(this.amout, deliveryShippingDeliveryModesPrice.amout) &&
        Objects.equals(this.currency, deliveryShippingDeliveryModesPrice.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amout, currency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryShippingDeliveryModesPrice {\n");
    
    sb.append("    amout: ").append(toIndentedString(amout)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

