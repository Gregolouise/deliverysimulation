package com.adeo.bomp.deliverysimulation.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Simulation Result for a User Cart
 */
@ApiModel(description = "Simulation Result for a User Cart")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T13:48:48.685Z")

public class DeliverySimulationResult   {
  @JsonProperty("solutions")
  @Valid
  private List<DeliverySolution> solutions = null;

  public DeliverySimulationResult solutions(List<DeliverySolution> solutions) {
    this.solutions = solutions;
    return this;
  }

  public DeliverySimulationResult addSolutionsItem(DeliverySolution solutionsItem) {
    if (this.solutions == null) {
      this.solutions = new ArrayList<DeliverySolution>();
    }
    this.solutions.add(solutionsItem);
    return this;
  }

  /**
   * Get solutions
   * @return solutions
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DeliverySolution> getSolutions() {
    return solutions;
  }

  public void setSolutions(List<DeliverySolution> solutions) {
    this.solutions = solutions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliverySimulationResult deliverySimulationResult = (DeliverySimulationResult) o;
    return Objects.equals(this.solutions, deliverySimulationResult.solutions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(solutions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliverySimulationResult {\n");
    
    sb.append("    solutions: ").append(toIndentedString(solutions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

