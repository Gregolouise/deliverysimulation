package com.adeo.bomp.deliverysimulation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OfferException extends RuntimeException {
    public OfferException(String s) {
        super(s);
    }
}
