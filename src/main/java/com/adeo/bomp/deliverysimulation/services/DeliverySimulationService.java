package com.adeo.bomp.deliverysimulation.services;


import com.adeo.bomp.deliverysimulation.exception.OfferException;
import com.adeo.bomp.deliverysimulation.model.mirakl.sh02.MiraklSH02Response;
import com.adeo.bomp.deliverysimulation.model.mirakl.sh02.MiraklSH02ResponseOrder;
import com.adeo.bomp.deliverysimulation.swagger.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DeliverySimulationService {

    private final RestTemplate restTemplate;
    @Autowired
    private Environment env;

    public DeliverySimulationService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();

    }

    public DeliverySimulationResult executeSimulation(DeliverySimulationRequest deliverySimulationRequest) {

        String countryName = deliverySimulationRequest.getDeliveryplace().getDeliveryAddress().getCountryCode();
        // TODO revoir ici!
        if (!StringUtils.equalsIgnoreCase(countryName, "DOM1")) {
            throw new OfferException("Wrong Country");
        }
        String countryCode = "DOM1";
        String url = env.getProperty("miraklSH02Url");
        String offers = "";
        for ( OfferCartItem offerItem : deliverySimulationRequest.getOffers()) {
            offers = offers + offerItem.getId() + "|" + offerItem.getQuantity() + ",";
        }
        offers = StringUtils.chop(offers);
        url = url +"?offers="+offers+"&shipping_zone_code="+countryCode;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", env.getProperty("miraklAuth"));
        headers.add("Accept", "application/json");

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<MiraklSH02Response> call = this.restTemplate.exchange(url, HttpMethod.GET, entity, MiraklSH02Response.class);
        System.out.println(call.getBody());

        // Here starts the mapping
        DeliverySimulationResult response = new DeliverySimulationResult();
        response.setSolutions(new ArrayList<>());
        DeliverySolution deliverySolution = new DeliverySolution();
        DeliveryShipping deliveryShipping = new DeliveryShipping();
        DeliveryShippingItems deliveryShippingItems = new DeliveryShippingItems();

//        List<MiraklSH02ResponseOrder> orders = new ArrayList<>();
//        for (MiraklSH02ResponseOrder currentOrder : call.getBody().getOrders()) {
//            currentOrder.
//        }
//        response.setSolution(new ArrayList<>());
////        response.setSolution(call);
////        response.setCounter(call.getBody().getTotalCount());
//        List<MiraklSH02ResponseOrder> orders = new ArrayList<>();
//        for (MiraklSH02ResponseOrder currentOrder : call.getBody().getOrders()) {
//            System.out.println("currentorder=="+currentOrder);
//            String shopName = currentOrder.getShopName();
//            ShippingSolution shippingSolution = new ShippingSolution();
//
//            //shipping
//            Shipping shipping = new Shipping();
//
//            //serviceLevel
//            ServiceLevel serviceLevel = new ServiceLevel();
//            serviceLevel.setName(currentOrder.getShopName());
//            serviceLevel.setReference(currentOrder.getCurrencyIsoCode());
//            Offer singleoffer = new Offer();
//            for(MiraklSH02ResponseOrderOffers offer : currentOrder.getOffers()){
//                singleoffer.setOfferId(offer.getOfferId());
//                singleoffer.setQuantity(offer.getOfferQuantity());
//            }
//            shipping.getOffer().add(singleoffer);
//            shipping.getServiceLevels().add(serviceLevel);
//            //end serviceLevel
//            shippingSolution.getShippingList().add(shipping);
//            //end shipping
//
//            response.getSolution().add(shippingSolution);
//        }
//
////        response.setOrders();
//        System.out.println("orders==="+orders);
//        System.out.println("wtf===="+response);
////        System.out.println("wtf get===="+response.getSolutionResponse());
//        return response;

        return response;
    }
}
