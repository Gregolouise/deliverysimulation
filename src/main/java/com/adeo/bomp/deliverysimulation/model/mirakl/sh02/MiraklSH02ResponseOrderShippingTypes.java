package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;
@MappedSuperclass

public class MiraklSH02ResponseOrderShippingTypes {

    @JsonProperty("code")
    private String code;
    @JsonProperty("label")
    private String label;
    @JsonProperty("total_shipping_price")
    private Number totalShippingPrice;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderShippingTypesShippingAdditionalFields.class)
    @JsonProperty(value="shipping_additional_fields")
    private List<MiraklSH02ResponseOrderShippingTypesShippingAdditionalFields> shippingAdditionalFields;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderShippingTypes{" +
                "code='" + code + '\'' +
                ", label='" + label + '\'' +
                ", totalShippingPrice=" + totalShippingPrice +
                ", shippingAdditionalFields=" + shippingAdditionalFields +
                '}';
    }
}
