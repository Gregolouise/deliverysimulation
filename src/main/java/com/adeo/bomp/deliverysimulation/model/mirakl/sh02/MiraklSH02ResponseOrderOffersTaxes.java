package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderOffersTaxes {
    @JsonProperty("amount")
    private double amount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersTaxes{" +
                "amount=" + amount +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
