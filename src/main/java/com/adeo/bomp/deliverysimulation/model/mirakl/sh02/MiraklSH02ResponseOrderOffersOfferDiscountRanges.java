package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderOffersOfferDiscountRanges {
    @JsonProperty("price")
    private double price;
    @JsonProperty("quantity_threshold")
    private int quantityThreshold;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersOfferDiscountRanges{" +
                "price=" + price +
                ", quantityThreshold=" + quantityThreshold +
                '}';
    }
}
