package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;
@MappedSuperclass
public class MiraklSH02ResponseOrderOffersOfferDiscount {
    @JsonProperty("discount_price")
    private double discountPrice;

    @JsonProperty("end_date")
    private String endDate;
    @JsonProperty("origin_price")
    private double originPrice;
    @JsonProperty("start_date")
    private String startDate;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersOfferDiscountRanges.class)
    @JsonProperty(value="ranges")
    private List<MiraklSH02ResponseOrderOffersOfferDiscountRanges> ranges;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersOfferDiscount{" +
                "discountPrice=" + discountPrice +
                ", endDate='" + endDate + '\'' +
                ", originPrice=" + originPrice +
                ", startDate='" + startDate + '\'' +
                ", ranges=" + ranges +
                '}';
    }
}
