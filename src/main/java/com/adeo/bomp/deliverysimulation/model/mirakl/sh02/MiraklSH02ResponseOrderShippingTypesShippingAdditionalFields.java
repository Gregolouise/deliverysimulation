package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderShippingTypesShippingAdditionalFields {

    @JsonProperty("code")
    private String code;
    @JsonProperty("value")
    private String value;
    @JsonProperty("type")
    private String type;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderShippingTypesShippingAdditionalFields{" +
                "code='" + code + '\'' +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
