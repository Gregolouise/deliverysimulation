package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderOffersPromotionsConfiguration {

    @JsonProperty("amount_off")
    private Number amountOff;
    @JsonProperty("free_items_quantity")
    private int freeItemsQuantity;
    @JsonProperty("internal_description")
    private String internalDescription;
    @JsonProperty("percentage_off")
    private Number percentageOff;
    @JsonProperty("type")
    private String type;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersPromotionsConfiguration{" +
                "amountOff=" + amountOff +
                ", freeItemsQuantity=" + freeItemsQuantity +
                ", internalDescription='" + internalDescription + '\'' +
                ", percentageOff=" + percentageOff +
                ", type='" + type + '\'' +
                '}';
    }
}
