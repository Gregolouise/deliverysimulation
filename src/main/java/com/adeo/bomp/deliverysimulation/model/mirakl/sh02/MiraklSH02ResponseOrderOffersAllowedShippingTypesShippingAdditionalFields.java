package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderOffersAllowedShippingTypesShippingAdditionalFields {

    @JsonProperty("code")
    private String code;

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private String value;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersAllowedShippingTypesShippingAdditionalFields{" +
                "code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
