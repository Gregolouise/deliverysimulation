package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import java.util.Map;

@MappedSuperclass

public class MiraklSH02ResponseOrderPromotionsAppliedPromotions {

    @JsonProperty(value="apportioned")
    private Boolean apportioned;
    @JsonProperty(value="deduced_amount")
    private Number deducedAmount;
    @JsonProperty(value="id")
    private String id;
    @JsonProperty(value="offered_quantity")
    private int offeredQuantity;


    @JsonProperty(value="configuration")
    private Map configuration;
    @JsonAnyGetter
    private Map getConfiguration() {
        return configuration;
    };

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderPromotionsAppliedPromotions{" +
                "apportioned=" + apportioned +
                ", deducedAmount=" + deducedAmount +
                ", id='" + id + '\'' +
                ", offeredQuantity=" + offeredQuantity +
                ", configuration=" + configuration +
                '}';
    }
}
