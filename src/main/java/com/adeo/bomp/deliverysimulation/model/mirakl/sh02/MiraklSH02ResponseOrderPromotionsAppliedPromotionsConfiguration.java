package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderPromotionsAppliedPromotionsConfiguration {

    @JsonProperty(value="amount_off")
    private Number amountOff;
    @JsonProperty(value="free_items_quantity")
    private int freeItemsQuantity;
    @JsonProperty(value="internal_description")
    private String internalDescription;
    @JsonProperty(value="percentage_off")
    private Number percentageOff;
    @JsonProperty(value="type")
    private String type;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderPromotionsAppliedPromotionsConfiguration{" +
                "amountOff=" + amountOff +
                ", freeItemsQuantity=" + freeItemsQuantity +
                ", internalDescription='" + internalDescription + '\'' +
                ", percentageOff=" + percentageOff +
                ", type='" + type + '\'' +
                '}';
    }
}
