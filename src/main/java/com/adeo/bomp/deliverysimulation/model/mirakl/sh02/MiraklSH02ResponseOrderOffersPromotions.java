package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;
@MappedSuperclass

public class MiraklSH02ResponseOrderOffersPromotions {
    @JsonProperty("apportioned")
    private Boolean apportioned;
    @JsonProperty("deduced_amount")
    private Number deducedAmount;
    @JsonProperty("id")
    private String id;
    @JsonProperty("offered_quantity")
    private int offeredQuantity;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersPromotionsConfiguration.class)
    @JsonProperty(value="configuration")
    private List<MiraklSH02ResponseOrderOffersPromotionsConfiguration> configuration;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersPromotions{" +
                "apportioned=" + apportioned +
                ", deducedAmount=" + deducedAmount +
                ", id='" + id + '\'' +
                ", offeredQuantity=" + offeredQuantity +
                ", configuration=" + configuration +
                '}';
    }
}
