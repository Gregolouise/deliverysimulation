package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@MappedSuperclass
public class MiraklSH02ResponseOrder {
    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    @JsonProperty("shop_id")
    private String shopId;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @JsonProperty("shop_name")
    private String shopName;


    @OneToMany(targetEntity = MiraklSH02ResponseOrderShippingTypes.class)
    @JsonProperty(value="shipping_types")
    private List<MiraklSH02ResponseOrderShippingTypes> ShippingTypes;


////    @OneToMany(targetEntity = MiraklSH02ResponseOrderSelectedShippingType.class)
    @JsonProperty(value="selected_shipping_type")
    private Map selectedShippingType;
//    @JsonAnyGetter
    private Map getSelectedShippingType() {
        return selectedShippingType;
    }
//
//
//    @OneToMany(targetEntity = MiraklSH02ResponseOrderPromotions.class)

    @JsonProperty(value="promotions")
    private Map promotions;
//    @JsonAnyGetter
    private Map getPromotions() {
        return promotions;
    };



    public List<MiraklSH02ResponseOrderOffers> getOffers() {
        return offers;
    }

    public void setOffers(List<MiraklSH02ResponseOrderOffers> offers) {
        this.offers = offers;
    }

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffers.class)
    @JsonProperty(value="offers")
    private List<MiraklSH02ResponseOrderOffers> offers;

    @JsonProperty(value="leadtime_to_ship")
    private int leadtimeToShip;

    @JsonProperty(value="currency_iso_code")
    private String currencyIsoCode;

    @JsonProperty(value="channel_code")
    private String channelCode;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrder{" +
                "shopId='" + shopId + '\'' +
                ", shopName='" + shopName + '\'' +
                ", ShippingTypes=" + ShippingTypes +
                ", selectedShippingType=" + selectedShippingType +
                ", promotions=" + promotions +
                ", offers=" + offers +
                ", leadtimeToShip=" + leadtimeToShip +
                ", currencyIsoCode='" + currencyIsoCode + '\'' +
                ", channelCode='" + channelCode + '\'' +
                '}';
    }

    public List<MiraklSH02ResponseOrderShippingTypes> getShippingTypes() {
        return ShippingTypes;
    }

    public void setShippingTypes(List<MiraklSH02ResponseOrderShippingTypes> shippingTypes) {
        ShippingTypes = shippingTypes;
    }

    public void setSelectedShippingType(Map selectedShippingType) {
        this.selectedShippingType = selectedShippingType;
    }

    public void setPromotions(Map promotions) {
        this.promotions = promotions;
    }

    public int getLeadtimeToShip() {
        return leadtimeToShip;
    }

    public void setLeadtimeToShip(int leadtimeToShip) {
        this.leadtimeToShip = leadtimeToShip;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}
