package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@MappedSuperclass
public class MiraklSH02ResponseOrderOffers {


    @JsonProperty("allow_quote_requests")
    private Boolean allowQuoteRequests;

    @JsonProperty("line_only_shipping_price")
    private double lineOnlyShippingPrice;

    @JsonProperty("line_only_total_price")
    private double lineOnlyTotalPrice;

    @JsonProperty("line_original_quantity")
    private int lineOriginalQuantity;

    @JsonProperty("line_price")
    private double linePrice;

    @JsonProperty("line_quantity")
    private int lineQuantity;

    @JsonProperty("line_shipping_price")
    private double lineShippingPrice;

    @JsonProperty("line_total_price")
    private double lineTotalPrice;

    @JsonProperty("offer_price")
    private double offerPrice;

    @JsonProperty("offer_quantity")
    private int offerQuantity;

    @JsonProperty("offer_id")
    private int offerId;

    @JsonProperty("product_category_code")
    private String productCategoryCode;

    @JsonProperty("product_tax_code")
    private String productTaxCode;

    @JsonProperty("shipping_price_additional_unit")
    private double shippingPriceAdditionalUnit;

    @JsonProperty("shipping_price_unit")
    private double shippingPriceUnit;



    public List<MiraklSH02ResponseOrderOffersAllowedShippingTypes> getAllowedShippingTypes() {
        return allowedShippingTypes;
    }

    public void setAllowedShippingTypes(List<MiraklSH02ResponseOrderOffersAllowedShippingTypes> allowedShippingTypes) {
        this.allowedShippingTypes = allowedShippingTypes;
    }

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersAllowedShippingTypes.class)
    @JsonProperty(value="allowed_shipping_types")
    private List<MiraklSH02ResponseOrderOffersAllowedShippingTypes> allowedShippingTypes;

//    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersOfferDiscount.class)
//    @JsonProperty(value="offer_discount")
//    private List<MiraklSH02ResponseOrderOffersOfferDiscount> OfferDiscount;

    @JsonProperty(value="offer_discount")
    private Map OfferDiscount;
    @JsonAnyGetter
    private Map getOfferDiscount() {
        return OfferDiscount;
    };


    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersOfferAdditionalFields.class)
    @JsonProperty(value="offer_additional_fields")
    private List<MiraklSH02ResponseOrderOffersOfferAdditionalFields> offerAdditionalFields;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersTaxes.class)
    @JsonProperty(value="taxes")
    private List<MiraklSH02ResponseOrderOffersTaxes> taxes;

    public List<MiraklSH02ResponseOrderOffersPromotions> getPromotions() {
        return promotions;
    }

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersPromotions.class)
    @JsonProperty(value="promotions")
    private List<MiraklSH02ResponseOrderOffersPromotions> promotions;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersShippingTaxes.class)
    @JsonProperty(value="shipping_taxes")
    private List<MiraklSH02ResponseOrderOffersShippingTaxes> shippingTaxes;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffers{" +
                "allowQuoteRequests=" + allowQuoteRequests +
                ", lineOnlyShippingPrice=" + lineOnlyShippingPrice +
                ", lineOnlyTotalPrice=" + lineOnlyTotalPrice +
                ", lineOriginalQuantity=" + lineOriginalQuantity +
                ", linePrice=" + linePrice +
                ", lineQuantity=" + lineQuantity +
                ", lineShippingPrice=" + lineShippingPrice +
                ", lineTotalPrice=" + lineTotalPrice +
                ", offerPrice=" + offerPrice +
                ", offerQuantity=" + offerQuantity +
                ", offerId=" + offerId +
                ", productCategoryCode='" + productCategoryCode + '\'' +
                ", productTaxCode='" + productTaxCode + '\'' +
                ", shippingPriceAdditionalUnit=" + shippingPriceAdditionalUnit +
                ", shippingPriceUnit=" + shippingPriceUnit +
                ", allowedShippingTypes=" + allowedShippingTypes +
                ", OfferDiscount=" + OfferDiscount +
                '}';
    }

    public Boolean getAllowQuoteRequests() {
        return allowQuoteRequests;
    }

    public void setAllowQuoteRequests(Boolean allowQuoteRequests) {
        this.allowQuoteRequests = allowQuoteRequests;
    }

    public double getLineOnlyShippingPrice() {
        return lineOnlyShippingPrice;
    }

    public void setLineOnlyShippingPrice(double lineOnlyShippingPrice) {
        this.lineOnlyShippingPrice = lineOnlyShippingPrice;
    }

    public double getLineOnlyTotalPrice() {
        return lineOnlyTotalPrice;
    }

    public void setLineOnlyTotalPrice(double lineOnlyTotalPrice) {
        this.lineOnlyTotalPrice = lineOnlyTotalPrice;
    }

    public int getLineOriginalQuantity() {
        return lineOriginalQuantity;
    }

    public void setLineOriginalQuantity(int lineOriginalQuantity) {
        this.lineOriginalQuantity = lineOriginalQuantity;
    }

    public double getLinePrice() {
        return linePrice;
    }

    public void setLinePrice(double linePrice) {
        this.linePrice = linePrice;
    }

    public int getLineQuantity() {
        return lineQuantity;
    }

    public void setLineQuantity(int lineQuantity) {
        this.lineQuantity = lineQuantity;
    }

    public double getLineShippingPrice() {
        return lineShippingPrice;
    }

    public void setLineShippingPrice(double lineShippingPrice) {
        this.lineShippingPrice = lineShippingPrice;
    }

    public double getLineTotalPrice() {
        return lineTotalPrice;
    }

    public void setLineTotalPrice(double lineTotalPrice) {
        this.lineTotalPrice = lineTotalPrice;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getOfferQuantity() {
        return offerQuantity;
    }

    public void setOfferQuantity(int offerQuantity) {
        this.offerQuantity = offerQuantity;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getProductCategoryCode() {
        return productCategoryCode;
    }

    public void setProductCategoryCode(String productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }

    public String getProductTaxCode() {
        return productTaxCode;
    }

    public void setProductTaxCode(String productTaxCode) {
        this.productTaxCode = productTaxCode;
    }

    public double getShippingPriceAdditionalUnit() {
        return shippingPriceAdditionalUnit;
    }

    public void setShippingPriceAdditionalUnit(double shippingPriceAdditionalUnit) {
        this.shippingPriceAdditionalUnit = shippingPriceAdditionalUnit;
    }

    public double getShippingPriceUnit() {
        return shippingPriceUnit;
    }

    public void setShippingPriceUnit(double shippingPriceUnit) {
        this.shippingPriceUnit = shippingPriceUnit;
    }

    public void setOfferDiscount(Map offerDiscount) {
        OfferDiscount = offerDiscount;
    }

    public List<MiraklSH02ResponseOrderOffersOfferAdditionalFields> getOfferAdditionalFields() {
        return offerAdditionalFields;
    }

    public void setOfferAdditionalFields(List<MiraklSH02ResponseOrderOffersOfferAdditionalFields> offerAdditionalFields) {
        this.offerAdditionalFields = offerAdditionalFields;
    }

    public List<MiraklSH02ResponseOrderOffersTaxes> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<MiraklSH02ResponseOrderOffersTaxes> taxes) {
        this.taxes = taxes;
    }

    public void setPromotions(List<MiraklSH02ResponseOrderOffersPromotions> promotions) {
        this.promotions = promotions;
    }

    public List<MiraklSH02ResponseOrderOffersShippingTaxes> getShippingTaxes() {
        return shippingTaxes;
    }

    public void setShippingTaxes(List<MiraklSH02ResponseOrderOffersShippingTaxes> shippingTaxes) {
        this.shippingTaxes = shippingTaxes;
    }
}
