package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;
@MappedSuperclass

public class MiraklSH02ResponseOrderPromotions {

    @JsonProperty(value="total_deduced_amount")
    private Number totalDeducedAmount;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderPromotionsAppliedPromotions.class)
    @JsonProperty(value="applied_promotions")
    private List<MiraklSH02ResponseOrderPromotionsAppliedPromotions> appliedPromotions;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderPromotions{" +
                "totalDeducedAmount=" + totalDeducedAmount +
                ", appliedPromotions=" + appliedPromotions +
                '}';
    }
}
