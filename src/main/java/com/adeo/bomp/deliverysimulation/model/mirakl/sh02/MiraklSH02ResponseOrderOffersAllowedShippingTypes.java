package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;

@MappedSuperclass
public class MiraklSH02ResponseOrderOffersAllowedShippingTypes {

    @JsonProperty("code")
    private String code;

    @JsonProperty("label")
    private String label;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderOffersAllowedShippingTypesShippingAdditionalFields.class)
    @JsonProperty(value="shipping_additional_fields")
    private List<MiraklSH02ResponseOrderOffersAllowedShippingTypesShippingAdditionalFields> shippingAdditionalFields;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersAllowedShippingTypes{" +
                "code='" + code + '\'' +
                ", label='" + label + '\'' +
                ", shippingAdditionalFields=" + shippingAdditionalFields +
                '}';
    }
}
