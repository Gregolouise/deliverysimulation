package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;
@MappedSuperclass

public class MiraklSH02ResponseOrderSelectedShippingType {

    @JsonProperty("code")
    private String code;
    @JsonProperty("label")
    private String label;

    @OneToMany(targetEntity = MiraklSH02ResponseOrderSelectedShippingTypeShippingAdditionalFields.class)
    @JsonProperty(value="shipping_additional_fields")
    private List<MiraklSH02ResponseOrderSelectedShippingTypeShippingAdditionalFields> shippingAdditionalFields;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderSelectedShippingType{" +
                "code='" + code + '\'' +
                ", label='" + label + '\'' +
                ", shippingAdditionalFields=" + shippingAdditionalFields +
                '}';
    }
}
