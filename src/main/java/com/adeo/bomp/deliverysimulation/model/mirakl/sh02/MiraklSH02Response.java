package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;

@MappedSuperclass
public class MiraklSH02Response {
    public List<MiraklSH02ResponseOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<MiraklSH02ResponseOrder> orders) {
        this.orders = orders;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }


    @OneToMany(targetEntity = MiraklSH02ResponseOrder.class)
    @JsonProperty("orders")
    private List<MiraklSH02ResponseOrder> orders;

//    @JsonProperty("errors")
//    // TODO METTRE LA BONNE CLASS CORRESPONDANT AUX ERRORS
//    private String errors;

    @JsonProperty("total_count")
    private Integer totalCount;

    @Override
    public String toString() {
        return "MiraklSH02Response{" +
                "orders=" + orders +
                ", totalCount=" + totalCount +
                '}';
    }

//    public String getErrors() {
//        return errors;
//    }
//
//    public void setErrors(String errors) {
//        this.errors = errors;
//    }
}