package com.adeo.bomp.deliverysimulation.model.mirakl.sh02;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.MappedSuperclass;

@MappedSuperclass

public class MiraklSH02ResponseOrderOffersShippingTaxes {

    @JsonProperty("amount")
    private Number amount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;

    @Override
    public String toString() {
        return "MiraklSH02ResponseOrderOffersShippingTaxes{" +
                "amount=" + amount +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}


