package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class RelayPoint {
    private String system;
    private String code;

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "RelayPoint{" +
                "system='" + system + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
