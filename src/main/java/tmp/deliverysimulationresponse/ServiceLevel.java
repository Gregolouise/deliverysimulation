package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ServiceLevel {
    private String name;
    private String reference;
    private String deliveryDate;
    private Price price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ServiceLevel{" +
                "name='" + name + '\'' +
                ", reference='" + reference + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", price=" + price +
                '}';
    }
}
