package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DeliverySimulationResult {

    // TODO HERE GOES VAR STATIC

    private ShippingSolutionResponse solutionResponse;

    // TODO HERE GOES VAR INSTANCE

    // TODO HERE GOES CONSTRUCTORS

    // TODO HERE GOES BUSINESS FUNCTION

    @Override
    public String toString() {
        return "deliverysimulationresult{" +
                "solutionResponse=" + solutionResponse +
                '}';
    }

    // TODO HERE GOES GETTERS & SETTERS

    public ShippingSolutionResponse getSolutionResponse() {
        return solutionResponse;
    }

    public void setSolutionResponse(ShippingSolutionResponse solutionResponse) {
        this.solutionResponse = solutionResponse;
    }

}
