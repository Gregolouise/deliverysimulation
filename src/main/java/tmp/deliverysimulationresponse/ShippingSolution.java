package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;
import java.util.ArrayList;
import java.util.List;
@MappedSuperclass
public class ShippingSolution {
    private List<Shipping> shippingList = new ArrayList<>();

    public List<Shipping> getShippingList() {
        return shippingList;
    }

    public void setShippingList(List<Shipping> shippingList) {
        this.shippingList = shippingList;
    }

    @Override
    public String toString() {
        return "ShippingSolution{" +
                "shippingList=" + shippingList +
                '}';
    }
}
