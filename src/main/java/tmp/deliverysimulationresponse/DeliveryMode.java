package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DeliveryMode {
    private String deliveryMode;
    private String storeCode;
    private DeliveryAddress deliveryAddress;
    private RelayPoint relayPoint;

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public RelayPoint getRelayPoint() {
        return relayPoint;
    }

    public void setRelayPoint(RelayPoint relayPoint) {
        this.relayPoint = relayPoint;
    }

    @Override
    public String toString() {
        return "DeliveryMode{" +
                "deliveryMode='" + deliveryMode + '\'' +
                ", storeCode='" + storeCode + '\'' +
                ", deliveryAddress=" + deliveryAddress +
                ", relayPoint=" + relayPoint +
                '}';
    }
}
