package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;
import java.util.ArrayList;
import java.util.List;
@MappedSuperclass
public class Shipping {
    private List<Offer> Offer = new ArrayList<>();
    private DeliveryMode Mode;
    private List<ServiceLevel> ServiceLevels = new ArrayList<>();

    public List<tmp.deliverysimulationresponse.Offer> getOffer() {
        return Offer;
    }

    public void setOffer(List<tmp.deliverysimulationresponse.Offer> offer) {
        Offer = offer;
    }

    public DeliveryMode getMode() {
        return Mode;
    }

    public void setMode(DeliveryMode mode) {
        Mode = mode;
    }

    public List<ServiceLevel> getServiceLevels() {
        return ServiceLevels;
    }

    public void setServiceLevels(List<ServiceLevel> serviceLevels) {
        ServiceLevels = serviceLevels;
    }

    @Override
    public String toString() {
        return "ShippingSolution{" +
                "Offer=" + Offer +
                ", Mode=" + Mode +
                ", ServiceLevels=" + ServiceLevels +
                '}';
    }
}
