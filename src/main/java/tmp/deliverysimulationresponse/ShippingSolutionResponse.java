package tmp.deliverysimulationresponse;

import javax.persistence.MappedSuperclass;
import java.util.List;

@MappedSuperclass
public class ShippingSolutionResponse {
    private List<ShippingSolution> solution;

    public List<ShippingSolution> getSolution() {
        return solution;
    }

    public void setSolution(List<ShippingSolution> solution) {
        this.solution = solution;
    }

    @Override
    public String toString() {
        return "ShippingSolutionResponse{" +
                "solution=" + solution +
                '}';
    }
}

