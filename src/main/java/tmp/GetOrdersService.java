package tmp;

import tmp.restservices.GetOrdersRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GetOrdersService {

    @Autowired
    GetOrdersRestService getOrdersRestService;

    public ResponseEntity getOrders(String date) {
        return getOrdersRestService.getOrdersFromOR11(date);
    }
}

