package tmp;

import tmp.GetOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class GetOrdersController {
    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    String date = formatter.format(new Date(System.currentTimeMillis() - 300000));
//    date = formatter.format(date)
    @Autowired
GetOrdersService getOrdersService;

//    @Scheduled(cron="*/2 * * * * *")
    public HttpEntity getOrdersFromOR11() {
        System.out.println("CROOOOON");
        ResponseEntity call = getOrdersService.getOrders(date);
        System.out.println("call = "+call);
        return call;

    }
}
