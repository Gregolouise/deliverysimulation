//package com.example.deliverySimulation.web.restservices;
//
//import com.example.deliverySimulation.model.DeliverySimulationResult;
//import com.example.deliverySimulation.model.DeliverySimulationRequest;
//import com.example.deliverySimulation.model.bompResponse.MiraklSH02Response;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.http.*;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//import java.util.Map;
//import org.springframework.core.env.Environment;
//
//
//@Service
//public class RestService {
//    @Autowired Environment env;
//    private final RestTemplate restTemplate;
//
//    public RestService(RestTemplateBuilder restTemplateBuilder) {
//        this.restTemplate = restTemplateBuilder.build();
//    }
//
//    public DeliverySimulationResult getOffersFromSh02(DeliverySimulationRequest deliverySimulationRequest, String countryCode) {
//        String url = env.getProperty("miraklSH02Url");
//        String offers = "";
//        for (Map offerItem : deliverySimulationRequest.offers)
//            offers = offers + offerItem.get("id")+ "|" +offerItem.get("quantity")+",";
//        offers = StringUtils.chop(offers);
//        url = url +"?offers="+offers+"&shipping_zone_code="+countryCode;
//        System.out.println("url = " + url);
//
//        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.add("Authorization", env.getProperty("miraklAuth"));
//        headers.add("Accept", "application/json");
//
//        HttpEntity<String> entity = new HttpEntity<String>(headers);
//        ResponseEntity<MiraklSH02Response> call = this.restTemplate.exchange(url, HttpMethod.GET, entity, MiraklSH02Response.class);
//        System.out.println("ici ===="+call.getBody());
//        DeliverySimulationResult response = new DeliverySimulationResult(call.getBody());
//        return response;
//
//    }
//}
