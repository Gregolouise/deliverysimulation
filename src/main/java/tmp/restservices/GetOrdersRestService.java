package tmp.restservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GetOrdersRestService {
    @Autowired
    Environment env;

    private final RestTemplate restTemplate;

    public GetOrdersRestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity getOrdersFromOR11(String date) {
        String url = env.getProperty("miraklOR11Url");

        url = url + "?start_update_date=" + date;

        System.out.println("url = " +url);
        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", env.getProperty("miraklAuth"));
        headers.add("Accept", "application/json");

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> call =  this.restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        System.out.println("call dans service = "+call.getBody());
        return call;
    };
}
