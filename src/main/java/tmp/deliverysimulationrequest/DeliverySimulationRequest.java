package tmp.deliverysimulationrequest;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;
import java.util.Map;

public class DeliverySimulationRequest {

    @Id
    @GeneratedValue
    private int id;

    public void setOffer(List<Map> offers) {
        this.offers = offers;
    }

    public void setLocation(Map<String, String> location) {
        this.location = location;
    }

    public List <Map> offers;

    public Map <String, String> location;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", offers=" + offers +
                ", location=" + location +
                '}';
    }
}
